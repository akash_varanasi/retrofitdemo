package com.howtodoinjava.demo;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface UserService {
    @POST("/prod4/api/v2/reports/query")
    public Call<SprinklrApiResponse> getUser(@Body RequestBody obj);

    @POST
    public Call<RefreshData> getToken(@Url String url ,@Body RequestBody obj);

}