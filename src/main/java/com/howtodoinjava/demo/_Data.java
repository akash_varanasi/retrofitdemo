package com.howtodoinjava.demo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class _Data {

    @SerializedName("headings")
    @Expose
    private List<String> headings = null;
    @SerializedName("rows")
    @Expose
    private List<List<String>> rows = null;

    public List<String> getHeadings() {
        return headings;
    }

    public void setHeadings(List<String> headings) {
        this.headings = headings;
    }

    public List<List<String>> getRows() {
        return rows;
    }

    public void setRows(List<List<String>> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("headings", headings).append("rows", rows).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(rows).append(headings).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof _Data)) {
            return false;
        }
        _Data rhs = ((_Data) other);

        return new EqualsBuilder().append(rows, rhs.rows).append(headings, rhs.headings).isEquals();
    }

}