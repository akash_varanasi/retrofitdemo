package com.howtodoinjava.demo;
import java.io.UnsupportedEncodingException;
import java.lang.*;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import java.io.IOException;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;
import retrofit2.converter.jackson.JacksonConverterFactory;
import com.fasterxml.jackson.databind.ObjectMapper;


/*
				This code is written by Akash Varanasi
				Date - 10/6/2020
				Sprinklr Integration - API client
			*/

public class SprinklrClient
{
	public static int ApiRequest(String access_token,String refresh_token,Long end_time,int flag) {
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		final String key = "tgxwyba96jhm3fsx6795t7fh";
		final String secret = "txbFpVGWd6QcT72Zh5SUrpRT2W9TfgH4D69RGZuyBy7Tgm6dc4zzBSWw9PypFFCx";
		final String accessToken = access_token;

		final OkHttpClient okHttpClient = new OkHttpClient.Builder()
				.addInterceptor(new Interceptor() {

					public okhttp3.Response intercept(Chain chain) throws IOException {
						Request request = chain.request();
						request = request.newBuilder()
								.addHeader("key", key)
								.addHeader("Authorization", "Bearer " + accessToken)
								.addHeader("Content/Type","Application/Json")
								.addHeader("Accept", "Application/Json")

								.build();

						okhttp3.Response response = chain.proceed(request);
						return response;
					}
				})
				.readTimeout(5000, TimeUnit.SECONDS)
				.connectTimeout(5000, TimeUnit.SECONDS)
				.addInterceptor(interceptor)
				.build();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl("https://api2.sprinklr.com/")
				.client(okHttpClient)
				.addConverterFactory(GsonConverterFactory.create())
                //.addConverterFactory(JacksonConverterFactory.create(objectMapper))
				.addConverterFactory(ScalarsConverterFactory.create())
				.build();
		final  Long MILLISECONDS_IN_DAY = 86400000L;
		Long start_time = end_time - MILLISECONDS_IN_DAY;
		UserService service = retrofit.create(UserService.class);
		String Request_String = "{\n    \"reportingEngine\": \"PLATFORM\",\n    \"report\": \"INBOUND_CASE\",\n    \"startTime\": \""+Long.toString(start_time)+"\",\n    \"endTime\": \""+Long.toString(end_time)+"\",\n    \"timeZone\": \"Asia/Kolkata\",\n    \"pageSize\": \"5\",\n    \"page\": \"2\",\n    \"groupBys\": [\n        {\n            \"heading\": \"CASE_ID\",\n            \"dimensionName\": \"CASE_ID\",\n            \"groupType\": \"FIELD\",\n            \"details\": null\n        },\n        {\n            \"heading\": \"ROOT_AUM_SN_CREATED_TIME\",\n            \"dimensionName\": \"ROOT_AUM_SN_CREATED_TIME\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"interval\": \"1d\"\n            }\n        },\n        {\n            \"heading\": \"DATE_TYPE_CASE_CREATION_TIME\",\n            \"dimensionName\": \"DATE_TYPE_CASE_CREATION_TIME\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"interval\": \"1d\"\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5cab4a52e4b0b04b0f3bd1da\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5cbd9890e4b0de2d3ff5778a\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"First_Assigned_Time(Static)_CUSTOM\",\n            \"dimensionName\": \"First_Assigned_Time(Static)_CUSTOM\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"interval\": \"1d\"\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c7fd206e4b04b0f02cf1271\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5caac439e4b0b04b0f291175\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"SOURCE_TYPE\",\n            \"dimensionName\": \"SOURCE_TYPE\",\n            \"groupType\": \"FIELD\",\n            \"details\": null\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c91d458e4b0ad52578cfeeb\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c91d49ae4b0ad52578d045d\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5cb06d89e4b0bda051fb7f19\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c7fdf23e4b01aa1fe2f1604\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5caae8d7e4b0b04b0f2d9e4c\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c7fdf22e4b01aa1fe2f152c\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c7fdf22e4b01aa1fe2f155f\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c875bace4b0ad5256aec78f\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c875c6ce4b0ad5256aeda20\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c91d4d7e4b0ad52578d0a18\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"Resolved_Time_CUSTOM\",\n            \"dimensionName\": \"Resolved_Time_CUSTOM\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"interval\": \"1d\"\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"spr_uc_status\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"LATEST_AUM_SN_CREATED_TIME\",\n            \"dimensionName\": \"LATEST_AUM_SN_CREATED_TIME\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"interval\": \"1d\"\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c875873e4b0ad5256ae6bbd\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5c876639e4b0ad5256afa6ec\",\n                \"isSecureField\": false\n            }\n        },\n        {\n            \"heading\": \"UNIFIED_MESSAGE_ID\",\n            \"dimensionName\": \"UNIFIED_MESSAGE_ID\",\n            \"groupType\": \"FIELD\",\n            \"details\": null\n        },\n        {\n            \"heading\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"dimensionName\": \"UNIVERSAL_CASE_CUSTOM_PROPERTY\",\n            \"groupType\": \"FIELD\",\n            \"details\": {\n                \"srcType\": \"CUSTOM\",\n                \"fieldName\": \"5caaabd1e4b0b04b0f27957d\",\n                \"isSecureField\": false\n            }\n        }\n    ],\n    \"projections\": [\n        {\n            \"heading\": \"OPERATIONAL_SOURCE_TO_CREATION\",\n            \"measurementName\": \"OPERATIONAL_SOURCE_TO_CREATION\",\n            \"aggregateFunction\": \"SUM\"\n        },\n        {\n            \"heading\": \"CASE_FIRST_RESPONSE_TIME\",\n            \"measurementName\": \"CASE_FIRST_RESPONSE_TIME\",\n            \"aggregateFunction\": \"AVG\"\n        },\n        {\n            \"heading\": \"OPERATIONAL_FIRST_RESPONSE_TIME\",\n            \"measurementName\": \"OPERATIONAL_FIRST_RESPONSE_TIME\",\n            \"aggregateFunction\": \"AVG\"\n        },\n        {\n            \"heading\": \"CASE_DURATION\",\n            \"measurementName\": \"CASE_DURATION\",\n            \"aggregateFunction\": \"SUM\"\n        },\n        {\n            \"heading\": \"CASE_COUNT\",\n            \"measurementName\": \"CASE_COUNT\",\n            \"aggregateFunction\": \"SUM\"\n        },\n        {\n            \"heading\": \"ASSOCIATED_BRAND_MESSAGE_COUNT\",\n            \"measurementName\": \"ASSOCIATED_BRAND_MESSAGE_COUNT\",\n            \"aggregateFunction\": \"SUM\"\n        },\n        {\n            \"heading\": \"ASSOCIATED_FAN_MESSAGE_COUNT\",\n            \"measurementName\": \"ASSOCIATED_FAN_MESSAGE_COUNT\",\n            \"aggregateFunction\": \"SUM\"\n        }\n    ],\n    \"filters\": []\n}";

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, Request_String);

		System.out.println("key : " + key);
		System.out.println("secret : " + secret);
		System.out.println("access_token : " + access_token);
		System.out.println("refresh_token :" + refresh_token);

		int count = 0;

        Call<SprinklrApiResponse> callSync = service.getUser(body);

		

		try {

			Response<SprinklrApiResponse> response = callSync.execute();
			System.out.println(response);
			SprinklrApiResponse apiResponse = response.body();
			System.out.println(apiResponse);

			if (response.code()==401){
				flag =0;
            }
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	return  flag;}

	public static String[] getting_token(String key,String secret,String refresh_token)throws MalformedURLException,
			UnsupportedEncodingException
	{
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		final OkHttpClient okHttpClient = new OkHttpClient.Builder()
				.addInterceptor(new Interceptor() {

					public okhttp3.Response intercept(Chain chain) throws IOException {
						Request request = chain.request();
						request = request.newBuilder()
								.addHeader("Content-Type", "application/x-www-form-urlencoded")
								.addHeader("Cookie", "AWSALB=miOmR1zgQ8cwnL5lqmohLtAP1COXeDm+uwKMBi6b72F89kDFDafe1sVgS2TLsMp+xCzt2qxNkroLO1T+wlN+zMcRADmHFLqhrst+zalA+kTxxQw8gahLTUhJ1WTG; AWSALBCORS=miOmR1zgQ8cwnL5lqmohLtAP1COXeDm+uwKMBi6b72F89kDFDafe1sVgS2TLsMp+xCzt2qxNkroLO1T+wlN+zMcRADmHFLqhrst+zalA+kTxxQw8gahLTUhJ1WTG")
								.build();

						okhttp3.Response response = chain.proceed(request);
						return response;
					}
				})
				.readTimeout(4000, TimeUnit.SECONDS)
				.connectTimeout(4000, TimeUnit.SECONDS)
				.addInterceptor(interceptor)
				.build();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl("https://api2.sprinklr.com/")
				.client(okHttpClient)
				.addConverterFactory(GsonConverterFactory.create())
				.addConverterFactory(ScalarsConverterFactory.create())
				.build();
		UserService service = retrofit.create(UserService.class);


		String encoded_token = URLEncoder.encode(refresh_token, "UTF-8") ;

		final String baseurl = "https://api2.sprinklr.com/prod4/oauth/token?";

		String url= baseurl+"client_id="+key+"&"+"client_secret="+secret+"&"+"redirect_uri=https://sprinklr.com/&grant_type=refresh_token"+"&"+"refresh_token="+encoded_token;

		final Logger LOGGER =
				Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		LOGGER.log(Level.INFO, "My first Log Message");

		System.out.println("key : " + key);
		System.out.println("secret : " + secret);
		System.out.println("refresh_token : " + refresh_token);
		System.out.println("encoded_token : " + encoded_token);
		MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
		RequestBody body = RequestBody.create(mediaType, "");

		System.out.println("This is for refresh token");
		System.out.println(url);
		System.out.println("The end of UserClientClient2");
		String[] tokens = new String[2];
		Call <RefreshData> callSync = service.getToken(url ,body);

		try {
			Response<RefreshData> response = callSync.execute();
			RefreshData apiResponse = response.body();
			System.out.println(apiResponse);
			tokens[0]=apiResponse.getAccessToken();
			tokens[1]=apiResponse.getRefreshToken();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return tokens;
	}
}
