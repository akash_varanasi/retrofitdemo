package com.howtodoinjava.demo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class SprinklrApiResponse {

	@SerializedName("data")
	@Expose
	private Data data;
	@SerializedName("errors")
	@Expose
	private List<Object> errors = null;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public List<Object> getErrors() {
		return errors;
	}

	public void setErrors(List<Object> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("data", data).append("errors", errors).toString();
	}

}