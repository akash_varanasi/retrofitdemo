package com.howtodoinjava.demo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ResponsePojo {

    @SerializedName("data")
    @Expose
    private _Data data;
    @SerializedName("errors")
    @Expose
    private List<Object> errors ;

    public _Data getData() {
        return data;
    }

    public void setData(_Data data) {
        this.data = data;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).append("errors", errors).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(data).append(errors).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ResponsePojo)) {
            return false;
        }
        ResponsePojo rhs = ((ResponsePojo) other);
        return new EqualsBuilder().append(data, rhs.data).append(errors, rhs.errors).isEquals();
    }

}