package com.howtodoinjava.demo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Data {

	@SerializedName("headings")
	@Expose
	private List<String> headings ;
	@SerializedName("rows")
	@Expose
	private List<List<Object>> rows = null;

	public List<String> getHeadings() {
		return headings;
	}

	public void setHeadings(List<String> headings) {
		this.headings = headings;
	}

	public List<List<Object>> getRows() {
		return rows;
	}

	public void setRows(List<List<Object>> rows) {
		this.rows = rows;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("headings", headings).append("rows", rows).toString();
	}

}